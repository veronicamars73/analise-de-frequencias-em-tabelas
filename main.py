import openpyxl
from pathlib import Path


def getColumns(sheet, cols):
    for column in sheet.iter_cols(1, sheet.max_column):
        cols.append((column[0].value).replace(" ","_"))

        
def dataDic(dic,cols,sheet):
    for i, row in enumerate(sheet.iter_rows(values_only=True)):
        if i == 0:
            for c in cols:
                dic[c]=[]
        else:
            for c in range(len(col_names)):
                dic[cols[c]].append(row[c])

def printDic(dic, mod):
    print(mod)
    print("")
    print(data)
    print("")

def datatoRol(dic, cols):
    for c in cols:
        dic[c]=sorted(dic[c])

def countFreq(data,freq):
    for d in data:
        if d in freq.keys():
            freq[d] = freq[d]+1
        else:
            freq[d] = 1

def countSheetFreq(data, cols, freq):
    for c in cols:
        aux = {}
        countFreq(data[c], aux)
        freq.append(aux)

xlsx_file = Path('recursos', 'C03-recursos_de_aprendizagem.xlsx')
wb_obj = openpyxl.load_workbook(xlsx_file) 
sheet = wb_obj.active

col_names = []
getColumns(sheet,col_names)   

data={}

dataDic(data,col_names,sheet)
            
printDic(data,"Dados Brutos:")

datatoRol(data,col_names)
    
printDic(data,"Rol de dados:")

freq=[]

countSheetFreq(data,col_names,freq)

print(freq)


